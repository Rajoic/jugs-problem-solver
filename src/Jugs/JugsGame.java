package Jugs;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;

//class for the game object
public class JugsGame {

    //integer arrays storing the initial state, the capacities, and the goal states of the jugs
    public int[] initial = new int[3];
    public int[] cap = new int[3];
    public int[] goal = new int[3];

    //2 ArrayLists for storing expanded and unexpanded nodes respectively
    public ArrayList<Node> UnexpNodes = new ArrayList<Node>();
    public ArrayList<Node> ExpNodes = new ArrayList<Node>();

    //2 integers for storing the amound of expanded and unexpanded nodes
    public int expanded = 0;
    public int unexpanded = 0;

    //boolean indicating if the solution has been found
    public boolean solutionFound;


    //creates a game object, holding information about the game through all the variables declared above
    public JugsGame (int[] jugsInitial, int[] jugsCap, int[] jugsGoal){
        this.initial = jugsInitial;
        this.cap = jugsCap;
        this.goal = jugsGoal;
        this.expanded = 0;
        this.unexpanded = 0;
        this.UnexpNodes = new ArrayList<Node>();
        this.ExpNodes = new ArrayList<Node>();
        this.solutionFound = false;
    }
}

