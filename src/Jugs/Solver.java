package Jugs;

import sun.invoke.util.Wrapper;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

//class for solving the jugs problem
public class Solver {

    int space; // for holding the space in a single jug
    int decantAmount; // for holding the amount to decant
    Node solution = null; // holds the solution node. starts at null since the solution hasn't been found yet
    Node successor; // holds the successor of the current node being expanded
    Node current; // holds teh current node being expanded

    // an object which starts to solve the jugs problem
    public Solver(Node a, JugsGame b) throws Exception{

        // calls the solve method and stores the solution. If a solution isn't found, it returns null
        this.solution = solve(a, b);

        File outFile = new File("output.txt"); // creates a file object to store the results in
        PrintWriter output = new PrintWriter(outFile); // creates a PrintWriter to write to the file
        solutionOut(solution, b, output); // starts outputting the solution to the console and the file
        output.close(); // closes the PrintWriter object
    }

    // takes a node and an ArrayList of nodes and checks if the node is in the array.
    //  for checking if the node is awaiting expansion or has already been expanded
    public boolean checker(Node a, ArrayList<Node> b) {
        //for each node in the ArrayList
        for (Node x : b) {
            //check if it matches Node a
            if (Arrays.equals(a.contents, x.contents)) {
                return true;
            }
        }
        return false;
    }

    //the method for solving the jugs problem. uses the initial node and the game object
    public Node solve(Node a, JugsGame b) {

        //sets the current node to the initial node
        current = a;

        while (b.UnexpNodes.size() > 0) {

            //creates a successor node which will hold the expansion of the current node being expanded
            successor = new Node (current.caps, current.contents, current);

            //in the successor node, decant jug A into jug B
            AtoB(successor);
            //if successor isnt the same as the current node, isn't awaiting expansion and has been expanded
            if ((!Arrays.equals(successor.contents, current.contents)) && (!checker(successor, b.ExpNodes) && !checker(successor, b.UnexpNodes))) {
                //check if it is equal to the goal
                if (Arrays.equals(successor.contents, b.goal)) {
                    //set the successor to the solution node
                    solution = successor;
                    //set the successor to the solution node
                    b.ExpNodes.add(solution);
                    //set the game state to solved
                    b.solutionFound = true;
                }
                //add successor to the list of unexpanded nodes
                b.UnexpNodes.add(successor);
            }

            //reset successor
            successor = new Node (current.caps, current.contents, current);
            //in the successor noce, decant jug A into jug C
            AtoC(successor);
            //if successor isnt the same as the current node, isn't awaiting expansion and has been expanded
            if ((!Arrays.equals(successor.contents, current.contents)) && (!checker(successor, b.ExpNodes) && !checker(successor, b.UnexpNodes))) {
                //check if it is equal to the goal
                if (Arrays.equals(successor.contents, b.goal)) {
                    //set the successor to the solution node
                    solution = successor;
                    //set the successor to the solution node
                    b.ExpNodes.add(solution);
                    //set the game state to solved
                    b.solutionFound = true;
                }
                //add successor to the list of unexpanded nodes
                b.UnexpNodes.add(successor);
            }

            //reset successor
            successor = new Node (current.caps, current.contents, current);
            //in the successor node, decant jug B into jug A
            BtoA(successor);
            //if successor isnt the same as the current node, isn't awaiting expansion and has been expanded
            if ((!Arrays.equals(successor.contents, current.contents)) && (!checker(successor, b.ExpNodes) && !checker(successor, b.UnexpNodes))) {
                //check if it is equal to the goal
                if (Arrays.equals(successor.contents, b.goal)) {
                    //set the successor to the solution node
                    solution = successor;
                    //set the successor to the solution node
                    b.ExpNodes.add(solution);
                    //set the game state to solved
                    b.solutionFound = true;
                }
                //add successor to the list of unexpanded nodes
                b.UnexpNodes.add(successor);
            }

            //reset successor
            successor = new Node (current.caps, current.contents, current);
            //in the successor node, decant jug B into jug C
            BtoC(successor);
            //if successor isnt the same as the current node, isn't awaiting expansion and has been expanded
            if ((!Arrays.equals(successor.contents, current.contents)) && (!checker(successor, b.ExpNodes) && !checker(successor, b.UnexpNodes))) {
                //check if it is equal to the goal
                if (Arrays.equals(successor.contents, b.goal)) {
                    //set the successor to the solution node
                    solution = successor;
                    //set the successor to the solution node
                    b.ExpNodes.add(solution);
                    //set the game state to solved
                    b.solutionFound = true;
                }
                //add successor to the list of unexpanded nodes
                b.UnexpNodes.add(successor);
            }

            //reset successor
            successor = new Node (current.caps, current.contents, current);
            //in the successor node, decant jug C into jug A
            CtoA(successor);
            //if successor isnt the same as the current node, isn't awaiting expansion and has been expanded
            if ((!Arrays.equals(successor.contents, current.contents)) && (!checker(successor, b.ExpNodes) && !checker(successor, b.UnexpNodes))) {
                //check if it is equal to the goal
                if (Arrays.equals(successor.contents, b.goal)) {
                    //set the successor to the solution node
                    solution = successor;
                    //set the successor to the solution node
                    b.ExpNodes.add(solution);
                    //set the game state to solved
                    b.solutionFound = true;
                }
                //add successor to the list of unexpanded nodes
                b.UnexpNodes.add(successor);
            }

            //reset successor
            successor = new Node (current.caps, current.contents, current);
            //in the successor node, decant jug C into jug B
            CtoB(successor);
            //if successor isnt the same as the current node, isn't awaiting expansion and has been expanded
            if ((!Arrays.equals(successor.contents, current.contents)) && (!checker(successor, b.ExpNodes) && !checker(successor, b.UnexpNodes))) {
                //check if it is equal to the goal
                if (Arrays.equals(successor.contents, b.goal)) {
                    //set the successor to the solution node
                    solution = successor;
                    //set the successor to the solution node
                    b.ExpNodes.add(solution);
                    //set the game state to solved
                    b.solutionFound = true;
                }
                //add successor to the list of unexpanded nodes
                b.UnexpNodes.add(successor);
            }

            //removing the expanded node from the unexpanded nodes array
            b.UnexpNodes.remove(0);
            //adding the expanded node into the the expanded nodes array
            b.ExpNodes.add(new Node(current));

            //if the solution has been found, break out of the loop
            if (b.solutionFound) {
                break;
            }

            //if the unexpanded node array isnt empty
            if (!b.UnexpNodes.isEmpty()) {
                //get the next node in the list. the node is chosen by depth first search (oldest node first)
                current = new Node(b.UnexpNodes.get(0));
            }
        }
        //getting the amount of nodes left to expand
        b.expanded = b.ExpNodes.size();
        //getting the amount of nodes expanded in total
        b.unexpanded = b.UnexpNodes.size();
        //returning the solution or null if it wansn't found
        return solution;
    }

    // method for decanting jug A into B
    public Node AtoB(Node a) {

        //calculating the space in jug B by taking the contents of the jug away from the capacity
        space = a.caps[1] - a.contents[1];
        //if the contents of jug A is more than the space available in jug B
        if (a.contents[0] > space) {
            //set the decant amount to the amount of space of jug B
            decantAmount = space;
        }

        //if the contents of jug A is less than or equal to the space available in jug B
        if (a.contents[0] <= space) {
            //set the decant amount to the contents of jug A
            decantAmount = a.contents[0];
        }

        //the decant amount is taken away from the contents of jug A
        a.contents[0] -= decantAmount;
        //the decant amount is added to the contents of jug B
        a.contents[1] += decantAmount;

        //return the result
        return new Node(a.caps, a.contents, a);
    }

    // method for decanting jug A into C
    public Node AtoC(Node a) {

        //calculating the space in jug C by taking the contents of the jug away from the capacity
        space = a.caps[2] - a.contents[2];
        //if the contents of jug A is more than the space available in jug C
        if (a.contents[0] > space) {
            //set the decant amount to the amount of space of jug C
            decantAmount = space;
        }

        //if the contents of jug A is less than or equal to the space available in jug C
        if (a.contents[0] <= space) {
            //set the decant amount to the contents of jug A
            decantAmount = a.contents[0];
        }

        //the decant amount is taken away from the contents of jug A
        a.contents[0] -= decantAmount;
        //the decant amount is added to the contents of jug C
        a.contents[2] += decantAmount;

        //return the result
        return new Node(a.caps, a.contents, a);
    }

    // method for decanting jug B into C
    public Node BtoC(Node a) {

        //calculating the space in jug C by taking the contents of the jug away from the capacity
        space = a.caps[2] - a.contents[2];
        //if the contents of jug B is more than the space available in jug C
        if (a.contents[1] > space) {
            //set the decant amount to the amount of space of jug C
            decantAmount = space;
        }

        //if the contents of jug B is less than or equal to the space available in jug C
        if (a.contents[1] <= space) {
            //set the decant amount to the contents of jug B
            decantAmount = a.contents[1];
        }

        //the decant amount is taken away from the contents of jug B
        a.contents[1] -= decantAmount;
        //the decant amount is added to the contents of jug C
        a.contents[2] += decantAmount;

        //return the result
        return new Node(a.caps, a.contents, a);
    }

    // method for decanting jug B into A
    public Node BtoA(Node a) {

        //calculating the space in jug A by taking the contents of the jug away from the capacity
        space = a.caps[0] - a.contents[0];
        //if the contents of jug B is more than the space available in jug A
        if (a.contents[1] > space) {
            //set the decant amount to the amount of space of jug A
            decantAmount = space;
        }

        //if the contents of jug B is less than or equal to the space available in jug A
        if (a.contents[1] <= space) {
            //set the decant amount to the contents of jug B
            decantAmount = a.contents[1];
        }

        //the decant amount is taken away from the contents of jug B
        a.contents[1] -= decantAmount;
        //the decant amount is added to the contents of jug A
        a.contents[0] += decantAmount;

        //return the result
        return new Node(a.caps, a.contents, a);
    }

    // method for decanting jug C into B
    public Node CtoB(Node a) {

        //calculating the space in jug B by taking the contents of the jug away from the capacity
        space = a.caps[1] - a.contents[1];
        //if the contents of jug C is more than the space available in jug B
        if (a.contents[2] > space) {
            //set the decant amount to the amount of space of jug B
            decantAmount = space;
        }

        //if the contents of jug C is less than or equal to the space available in jug B
        if (a.contents[2] <= space) {
            //set the decant amount to the contents of jug C
            decantAmount = a.contents[2];
        }

        //the decant amount is taken away from the contents of jug C
        a.contents[2] -= decantAmount;
        //the decant amount is added to the contents of jug B
        a.contents[1] += decantAmount;

        //return the result
        return new Node(a.caps, a.contents, a);
    }

    // method for decanting jug C into A
    public Node CtoA(Node a) {

        //calculating the space in jug A by taking the contents of the jug away from the capacity
        space = a.caps[0] - a.contents[0];
        //if the contents of jug C is more than the space available in jug A
        if (a.contents[2] > space) {
            //set the decant amount to the amount of space of jug A
            decantAmount = space;
        }

        //if the contents of jug C is less than or equal to the space available in jug A
        if (a.contents[2] <= space) {
            //set the decant amount to the contents of jug C
            decantAmount = a.contents[2];
        }

        //the decant amount is taken away from the contents of jug B
        a.contents[2] -= decantAmount;
        //the decant amount is added to the contents of jug A
        a.contents[0] += decantAmount;

        //return the result
        return new Node(a.caps, a.contents, a);
    }

    //prints the the node given and its parent until it gets to the initial node
    public static void printSolution(Node n, PrintWriter output) {
        //if the node has a parent recall printSolution using the parent
        if (n.parent != null) {
            printSolution(n.parent, output);
        }
        //prints Node n using the toString method in the Node class
        System.out.println(n.toString());
        //prints the node to the file
        output.println(n.toString());
    }

    //prints the solution details
    public void solutionOut (Node n, JugsGame x, PrintWriter output){
        //if not solution was found
        if (n == null) {
            //prints to console
            System.out.println("Capacities : " + Arrays.toString(x.cap)); //prints the capacities
            System.out.println("Initial State : " + Arrays.toString(x.initial)); //prints the initial state
            System.out.println("Goal State : " + Arrays.toString(x.goal)); //prints the goal state
            System.out.println("No solution"); //prints no solution
            System.out.println("Nodes Expanded : " + x.expanded); //prints the amount of nodes expanded
            System.out.println("Nodes Unexpanded : " + x.unexpanded); //prints the amount of nodes left unexpanded

            //prints to file the same details as above
            output.println("Capacities : " + Arrays.toString(x.cap));
            output.println("Initial State : " + Arrays.toString(x.initial));
            output.println("Goal State : " + Arrays.toString(x.goal));
            output.println("No solution");
            output.println("Nodes Expanded : " + x.expanded);
            output.println("Nodes Unexpanded : " + x.unexpanded);
        }
        //if a solution was found
        else {
            System.out.println("Solution found!\n"); //prints solution foudn
            output.println("Solution found!\n"); //prints solution found to the file
            printSolution(n, output); //runs print solution

            System.out.println("Moves Required : " + n.cost); //prints the cost to the solution
            System.out.println("Number of Nodes expanded: " + x.expanded); //prints the number of nodes expanded
            System.out.println("Number of Nodes left unexpanded: " + x.unexpanded); //prints the number of nodes left unexpanded
            System.out.println();

            //same as the above 4 println commands
            output.println("Moves Required : " + n.cost);
            output.println("Number of Nodes expanded: " + x.expanded);
            output.println("Number of Nodes left unexpanded: " + x.unexpanded);
            output.println();
        }
    }

    //takes a user input for the jugs initial state, goal state and jug capacities
    public static int[] inputJugs(int[] jugs, Scanner input) {

        int i = 0; //counts the jugs
        boolean error; //boolean for checking if an error occurred on input
        //for each jug
        for (int in : jugs) {
            error = true; //set error to true
            //while there is an error
            while (error) {
                try {
                    System.out.println("Please input the value for jug " + (i + 1) + " : ");
                    in = input.nextInt(); //take in a user's integer input
                    error = false; //if an error hasn't occurred, set error to false
                    jugs[i] = in; //set the current jug to the user input
                    i++; //increment the jug position
                } catch (InputMismatchException e) { //if an error has occurred on input
                    error = true; // set error to true
                    input.next(); //use up the current input token
                    //let the user know there was an error
                    System.out.println("ERROR: something other than an integer was entered. Try again.");
                }
            }
        }

        //return the filled array
        return jugs;
    }

    //program entry point
    public static void main(String[] args) throws Exception{

        int[] jugsInitial = new int[3]; //array of the jugs initial state
        int[] jugsCap = new int[3]; //array of the jugs capacities
        int[] jugsGoal = new int[3]; //array of the jugs goal state

        //creates an input scanner for user input
        Scanner input = new Scanner (System.in);

        System.out.println("3 integers are required for the capacities of the jugs");
        jugsCap = inputJugs(jugsCap, input); //populates the capacities array

        System.out.println("3 integers are required for the initial state of the jugs problem");
        boolean fits = false; //for checking if the initial state fits the capacities
        //while the initial state doest fit into the capacities
        while (!fits) {
            jugsInitial = inputJugs(jugsInitial, input); //populates the initial state array
            //check if each individual jug fits its capacity
            if ((jugsInitial[0] <= jugsCap[0]) && (jugsInitial[1] <= jugsCap[1]) && (jugsInitial[2] <= jugsCap[2])) {
                fits = true; //if it does, set fits to true
            }
            else {
                //tell the user it doesn't fit and make them try again
                System.out.println("The initial state doesn't fit into the capacities stated. Input the initial state again.");
            }
        }


        System.out.println("3 integers are required for the goal state of the jugs problem");
        fits = false; //for checking if the goal fits the capacities
        //while the goal state doesn't fit into the capacities
        while (!fits) {
            jugsGoal = inputJugs(jugsGoal, input); //populates the goal state array
            //check if each individual jug fits its capacity
            if ((jugsGoal[0] <= jugsCap[0]) && (jugsGoal[1] <= jugsCap[1]) && (jugsGoal[2] <= jugsCap[2])) {
                fits = true; //if it does, set fits to true
            }
            else {
                //tell the user it doesn't fit and make them try again
                System.out.println("The goal state doesn't fit into the capacities stated. Input the goal state again");
            }
        }

        input.close(); //closes the input scanner


        JugsGame game = new JugsGame(jugsInitial, jugsCap, jugsGoal); //creates a game object
        Node root = new Node(game.cap, game.initial); //creates a root node using the capacities and the initial state
        game.UnexpNodes.add(root); //adds the root node to unexpanded nodes
        new Solver(root, game); //creates a new solver object to start solving the problem
    }
}
