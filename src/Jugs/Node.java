package Jugs;

//class for the node object
public class Node {

    int youngestNode = 1; //holds the node number of the youngest node.
    int nodeNum; // holds the node number
    int cost; // holds the cost of a node. the cost is the how many moves it takes to get to that node from the initial state
    Node parent; // holds the parent node
    int[] contents; // holds the contents of the jugs
    int[] caps; // holds the capacities of the jugs

    // creates a node using another node. a constructor for making a copy of a node
    public Node (Node a) {
        this.nodeNum = a.nodeNum;
        this.contents = a.contents.clone();
        this.caps = a.caps.clone();
        this.cost = a.cost;
        // if the node is the initial node, it wont have a parent, so the parent is null
        if (a.parent == null) {
            this.parent = null;
        }
        else {
            this.parent = a.parent;
        }
    }

    //creates a node using just the capacities and the contents. For the initial node only since it wont have a parent
    public Node (int[] caps, int[] content){
        this.nodeNum = youngestNode + 1;
        this.contents = content.clone();
        this.caps = caps.clone();
        this.cost = 0;
        this.parent = null;

        youngestNode += 1;
    }

    //creates a node using the capacities, contents and a parent node. For nodes other than copies and the initial node
    // since they have to hold the parent
    public Node (int[] caps, int[] content, Node parent){
        this.nodeNum = youngestNode + 1;
        this.contents = content.clone();
        this.caps = caps.clone();
        this.cost = parent.cost + 1;
        this.parent = parent;

        youngestNode += 1;
    }


    /*
    public static Node duplicate (Node x) {

        if (x.parent == null) {
            return new Node(x.caps, x.contents);
        }
        else {
            return new Node(x.caps, x.contents, x.parent);
        }
    }
    */

    //toString method for Node objects. It outputs the contents of the jugs in the node.
    public String toString() {
        return "[" + contents[0] + " " + contents[1] + " " + contents[2] + "]";
    }
}
